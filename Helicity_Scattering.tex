\documentclass[preprint]{elsarticle}
%\documentclass[5p]{elsarticle}

\usepackage{amsmath}
\usepackage{amssymb}

\usepackage{graphicx}
\usepackage{physics}

\bibliographystyle{elsarticle-num}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newcommand{\vect}[1]{\mathbf{#1}}
\newcommand{\tensor}[1]{\widehat{#1}}

\newcommand{\sign}{\mathrm{sign}}

\newcommand{\bosi}{\boldsymbol{\sigma}}
\newcommand{\boga}{\boldsymbol{\gamma}}
\newcommand{\boal}{\boldsymbol{\alpha}}

\newcommand{\rmi}{\mathrm{i}}
\newcommand{\rme}{\mathrm{e}}

%% **** THERE SHOULD BE NONE OF THESE IN THE FINAL VERSION *****
%\newcommand{\tobedone}[1]{\textbf{*** \textit{ #1 }}}


\begin{document}

\title{Helicity-dependent scattering in layered Weyl semimetals}

\author{Mikhail Erementchouk\corref{cor}}
\ead{merement@gmail.com}
\author{Pinaki Mazumder}
\ead{pinakimazum@gmail.com}
\address{Department of Electrical Engineering and Computer Science, University of Michigan, Ann Arbor, MI 48109 USA}
\cortext[cor]{Corresponding author.}

\begin{abstract}
Weyl fermions are characterized by definite helicity, a relation between orientation of the spin and momentum. We investigate controlling helicity utilizing helicity-dependent dynamics due to separation between the Weyl points. We study transport properties of a multilayer structure formed by piece-wise constant scalar and vector potentials and separations between the Weyl points. We show that the transmission spectrum is determined by a relation between the electron momentum and momenta corresponding to Klein tunneling, when the barrier is fully transparent regardless of its width. The helicity dependence of the ``Klein momenta'' is shown to yield a helicity filtering effect.
\end{abstract}

\maketitle

\section{Introduction}

Experimental finding of non-stoichiometric crystals demonstrating properties of Weyl semimetals (WSM) \cite{yang_weyl_2015,xu_observation_2015,xu_discovery_2015,lv_observation_2015,lv_experimental_2015,lu_experimental_2015} sparked a significant interest in possible weyltronics applications, a special kind of devices employing specific features of electron excitations in WSM. Low-energy electron states in WSM behave as Weyl fermions, massless Dirac electrons, which is one of the main points of interest for novel applications \cite{shekhar_extremely_2015,wang_helicity-protected_2016}. Due to their massless character, Weyl fermions can be characterized by definite helicity, a tight relation between orientation of the electron spin and its momentum. An important property, which sets WSM apart from other Dirac materials is that the points of zero energy for particles with definite helicity, commonly known as Weyl points, are non-coinciding in WSM. This property together with topological protection of Weyl points makes helicity in WSM a promising candidate for carrying information in digital processing applications \cite{boechler_fundamental_2010,cavin_science_2012}.
At the same time, such applications require flexible control over helicity, which is a non-trivial task because in the ``classical'' limit there is no field directly coupled to helicity. This raises a general question about controllability of helicity in WSM.

From this perspective, a special attention is drawn to axial anomaly \cite{fujikawa_path_2004,ioffe_axial_2006,jackiw_axial_2010,liu_chiral_2013,basar_triangle_2014,burkov_chiral_2015}, a phenomenon of breaking chiral symmetry of a quantized Dirac field in parallel external electric and magnetic fields. As a result of this anomaly, there is a flux of particles between Weyl points with opposite chiralities \cite{gorbar_radiative_2013,hosur_recent_2013}. In the present paper, we explore an alternative approach based on the fact that separation of Weyl points in WSM induces different dynamics of states with different helicities thus making them distinguishable at the single-particle level. In order to reveal this effect, we study transport properties of WSM multilayer structures from the perspective of helicity dependent scattering. Similar systems have been studied previously \cite{bai_wavevector_2016,bai_chiral_2016} in the context of multilayer structures formed by modulated scalar potential with the main attention paid to mismatching boundaries between propagating and tunneling regimes of states with different helicities. In the present paper, we consider a general situation with piece-wise constant spatial distributions of scalar and vector potentials and separations between the Weyl points and show that the phenomenon of Klein tunneling leads to more distinguished manifestation of the helicity-dependent dynamics. 

The paper is organized as follows. In Section~\ref{sec:weyl_layered}, we adopt the equation of motion governing propagation of electron in WSM multilayer structures for application of the spinor transfer matrix approach and obtain the transfer matrix through a layer. In Section~\ref{sec:scattering}, we apply this result for deriving transmission and reflection coefficients of a three-layer structures. We show that as functions of the in-plane momentum they can be presented in a characteristic Lorentzian form with helicity dependent parameters. When penetration through the barrier is due to tunneling, the resonant in-plane momentum is complex and cannot be reached, so that the variation of scattering coefficients with the momentum is rather gradual. In the opposite case, the resonant momentum is real and when it is reached the barrier becomes transparent independently of its width. This Klein tunneling occurs at, generally speaking, oblique and helicity dependent directions. In Section~\ref{sec:helicity_filtering}, we show that this effect may lead to strong helicity filtering property. The main contribution of the paper is to establish a relation between prominent features in transmission spectra of WSM multilayers and the mutual arrangement of the Weyl points and to show that the helicity of electrons propagating through the structure can be controlled by external scalar and vector potentials.

\section{Weyl fermions in layered structures}
\label{sec:weyl_layered}

A layered WSM structure, schematically shown in Fig.~\ref{fig:layers}, is characterized by piece-wise constant parameters varying along one dimension while being constant along other two. Because of this, the in-plane components of the momentum constitute good quantum numbers, thereby effectively reducing the problem to one-dimensional. In turn, the spatial variation of the spinor wave function across the layers is conveniently described by spinor transfer matrices. This approach was presented for a 2D Dirac equation in \cite{erementchouk_dirac_2016} and here we only outline its extension for treating Weyl fermions.

\begin{figure}
  \centering
    \includegraphics[width=3.8in]{layers.png}
  \caption{(a) Geometry of a WSM layered system. (b) A three-layer system: a layer of width $d$ sandwiched between semi-infinite layers.}
  \label{fig:layers}
\end{figure}

A general equation describing Dirac electron in a layered medium  is 
\begin{equation}\label{eq:full_Dirac}
\begin{split}
 \rmi \gamma^0 & \left[ \partial_0 + \rmi e V(x)  + \rmi \gamma^5 q_0(x) \right] \psi = \\
 %
 & - \rmi v \boga \cdot 
        \left[ \nabla + \rmi e \vect{A}(x) + \rmi \gamma^5 \vect{q}(x) \right]\psi,
\end{split}
\end{equation}
with the piece-wise constant vector, $\vect{A}(x)$, and scalar, $V(x) $, potentials, and the separation between the Weyl points: $q_0(x)$ is the energy separation and $\vect{q}(x)$ is the separation in the momentum space. In order to emphasize the helicity property, we take the Dirac matrices $\boga$ in the chiral representation
\begin{equation}\label{eq:gamma_mats}
 \gamma^0 = \mqty(0 & \widehat{1} \\ \widehat{1} & 0), \qquad 
 \boga \equiv \gamma^i {\vect{e}}_i 
        = \mqty(0 & \bosi \\ -\bosi & 0).
\end{equation}
Within this representation, Eq.~\eqref{eq:full_Dirac} splits into two uncoupled equations describing states spanned by $\ket{L} = (\chi_+, 0, 0)^T $ and $\ket{R} = (0, 0, \chi_-)^T $, where $\chi_{\pm}$ are $2$-spinors. These are eigenspaces corresponding to eigenvalues $\xi = +1$ and $-1$, respectively, of the helicity projection operators
%\begin{equation}\label{eq:helicity_projectors}
 $P_{L,R} = \frac{1}{2} \left( 1 \pm \gamma^5 \right)$,
%\end{equation}
where $\gamma^5 = -\rmi \gamma^0 \gamma^1\gamma^2\gamma^3$ and in the chiral representation
\begin{equation}\label{eq:gamma5_mat}
\gamma^5 = \mqty(\widehat{1} & 0 \\ 0 & -\widehat{1}).
\end{equation}
Thus, $\ket{L} $ and $\ket{R} $ span particles with left and right helicities, respectively.

Eigenstates of Eq.~\eqref{eq:full_Dirac} are characterized by definite values of energy and the in-plain component of momentum, $\vect{P} = P_y \vect{e}_y + P_z \vect{e}_z$, so that
\begin{equation}\label{eq:psi_separation}
 \psi(x, y, z) = \psi(x) \exp \left( -\rmi \epsilon t + \rmi \vect{P} \cdot \vect{r} \right).
\end{equation}
Moreover, the projections of $\vect{A} $ and $\vect{q}$ on the $x$-axis can be excluded by the gauge transformation
\begin{equation}\label{eq:gauge_x}
 \psi(x) = \exp \left\{ -\rmi \int_{x_0}^x dx' \left[ eA_x(x') + \gamma^5 q_x(x') \right] \right\} \widetilde{\psi}(x).
\end{equation}
Thus, without losing generality we can assume that vectors $\vect{A} $ and $\vect{q} $ are oriented in the $(y, z)$-plane. This leads us to 
\begin{equation}\label{eq:eq_Weyl_WSM}
 \widetilde{\epsilon}(x)  \psi = -\rmi \alpha^1 \partial_x \psi + \boldsymbol{\alpha} \cdot \widetilde{\vect{P}}(x) \psi,
\end{equation}
where $\boldsymbol{\alpha} = \gamma^0 \boga $, $\widetilde{\epsilon}(x) = \epsilon - eV(x) -\gamma^5 q_0(x)$, $\widetilde{\vect{P}}(x) = \vect{P} + e \vect{A}(x) + \gamma^5 \vect{q}(x) $, and we have rescaled the energy and the scalar potential, so that, $v = 1$.

The spinor transfer matrix approach is based on representing solutions of Eq.~\eqref{eq:eq_Weyl_WSM} as superpositions of propagating spinor states. It should be emphasized that the spinor transfer matrix deals with spinors rather than amplitudes of the spinor wave function. This establishes a direct relation with the states relevant for incoming and outgoing channels and thus significantly simplifies description of transport properties.

For $\psi = \mqty(\chi_+ \\ \chi_-) $, we obtain
\begin{equation}\label{eq:eq_helicities}
 \widetilde{\epsilon}_\xi(x) \chi_\xi = \rmi \xi \sigma_x \partial_x \chi_\xi +
 \xi \bosi \cdot \widetilde{\vect{P}}_\xi \chi_\xi,
\end{equation}
where $\xi = \pm 1$ enumerates helicities, $\widetilde{\epsilon}_\xi(x) = \epsilon - U_\xi(x)$ with $U_\xi(x) = eV + \xi q_0(x)$ and $\widetilde{\vect{P}}_\xi(x) = \vect{P} + \vect{Q}_\xi(x)$ with $\vect{Q}_\xi(x) = e \vect{A}(x) + \xi \vect{q}(x) $. Within a layer, spinors corresponding to the states propagating along and against the $x$-axis, $\chi_\xi = \rme^{\pm \rmi k_\xi x} \ket{\widetilde{\chi}^{(\pm)}_\xi} $, satisfy equations similar to the equation for a spin $1/2$ particle in a magnetic field,
%\begin{equation}\label{eq:eff_spin}
$\widetilde{\epsilon}_\xi \ket{\widetilde{\chi}^{(\pm)}_\xi} = \bosi \cdot \vect{h}^{(\pm)}_\xi \ket{\widetilde{\chi}^{(\pm)}_\xi}$,
%\end{equation}
where the effective field is defined as
\begin{equation}\label{eq:eff_field}
 \vect{h}^{(\pm)}_\xi(k_\xi) = - \xi \left( \pm k_\xi \vect{e}_x + \widetilde{\vect{P}}_\xi \right). 
\end{equation}
Thus, the dispersion equation relating the local momentum $k_\xi$ and the energy of the Weyl fermion is
\begin{equation}\label{eq:disp_eq}
 \widetilde{\epsilon}_\xi = \pm {h}^{(\pm)}_\xi(k_\xi),
\end{equation}
with ${h}^{(\pm)}_\xi \equiv \sqrt{\vect{h}^{(\pm)}_\xi\cdot \vect{h}^{(\pm)}_\xi}$. It should be reminded that due to gauge transformation~\eqref{eq:gauge_x}, vector $\widetilde{\vect{P}}_\xi$ lies in the $(y,z) $-plane and, therefore, ${h}^{(+)}_\xi = {h}^{(-)}_\xi = h_\xi$. 

Solutions corresponding to different branches in Eq.~\eqref{eq:disp_eq} are conveniently presented in terms of spin coherent states $\ket{\pm \vect{h}_\xi^{(\pm)}} $. Usually, they are defined for vectors with real components by rotating the quantization axis to align it along the defining vector \cite{aravind_spin_1999,CombescureCoherent2012}. For the system under consideration, however, it is convenient to represent the spinors with the help of dilation operators \cite{erementchouk_dirac_2016}, emphasizing the symmetry of states $\ket{\vect{h}^{(\pm)}} $ and accounting for the case of imaginary $h_x$, which takes place when $k^2 < 0$. For $k_\xi^2 > 0$, we have
\begin{equation}\label{eq:states_prop}
\ket{\vect{h}^{(\pm)}_\xi} = K_\xi \exp \left( \beta \widetilde{\vect{P}}_\xi \cdot \bosi/2 \right) \ket{\pm \vect{e}_x},
\end{equation}
where $K_\xi = \sqrt{k_\xi/|\widetilde{\epsilon}_\xi|}$, $k_\xi \cosh(\beta \widetilde{P}_\xi) = |\widetilde{\epsilon}_\xi|$, $k_\xi \sinh(\beta \widetilde{P}_\xi) = \sign(\widetilde{\epsilon}_\xi)\widetilde{P}_\xi $. 

For the tunneling regime, when $k_\xi = \rmi \kappa_\xi$ with $\kappa_\xi > 0$, we can write
\begin{equation}\label{eq:states_att}
\ket{\vect{h}^{(\pm)}_\xi} = C_\xi \exp \left( \beta' \widetilde{\vect{P}}_\xi \cdot \bosi/2 \right) \ket{\pm \vect{l}_\xi},
\end{equation}
where $\vect{l}_\xi = \widetilde{\vect{P}}_\xi \times \vect{e}_x/\widetilde{P}_\xi$, $C_\xi =\sqrt{\kappa_\xi/\widetilde{P}_\xi} $, $\kappa_\xi \cosh(\beta' \widetilde{P}_\xi) =  \widetilde{P}_\xi$, and $\kappa_\xi \sinh(\beta' \widetilde{P}_\xi) = \widetilde{\epsilon}_\xi$. 

The spectral point of special importance, separating tunneling and propagating regimes, is $|\widetilde{\epsilon}_\xi| = \widetilde{P}_\xi $. At this point, $k_\xi = -k_\xi = 0$ and, therefore, states characterized by a definite momentum do not exhaust all solutions of Eq.~\eqref{eq:eq_helicities}, so that one needs to take into account the secular solution, which results in an algebraic decay of the transmission coefficient \cite{erementchouk_dirac_2016}. Since this  takes place only at isolated spectral points, we will assume that $|\widetilde{\epsilon}_\xi| \ne \widetilde{P}_\xi$.

For a three-layer structure with a layer of finite width $d$ sandwiched between semi-infinite layers (see Fig.~\ref{fig:layers}(b)), following the formalism developed in \cite{erementchouk_dirac_2016}, the transfer matrix through the middle layer is found to be
\begin{equation}\label{eq:tm_uniform}
 \widehat{T}_\xi = \exp \left( \rmi \widetilde{\epsilon} \sigma_x d - 
 \widetilde{P}_\xi \vect{l}_\xi \cdot \bosi d \right),
\end{equation}
regardless of the regime of propagation inside the layer. The same approach can be used for a multilayer structure yielding $ \widehat{T}_\xi = \prod_n \widehat{T}_\xi(n) $, where $\widehat{T}_\xi(n)$ is given by Eq.~\eqref{eq:tm_uniform} with $\widetilde{\epsilon}_\xi $, $d$, $\widetilde{P}_\xi $, and $\vect{l}_\xi $ taken for the $n$-th layer.

%%%%%%%%%%%

\section{Helicity-dependent scattering}
\label{sec:scattering}

One manifestation of helicity-dependent dynamics follows straightforwardly from Eq.~\eqref{eq:disp_eq} resolved with respect to the $x$-component of the momentum,
\begin{equation}\label{eq:sol_disp}
 k_\xi = \pm \sqrt{\widetilde{\epsilon}_\xi^2 - \widetilde{P}_\xi^2}.
\end{equation}
 For $F(\epsilon, \vect{P}) = (\epsilon - eV)q_0 + \left(\vect{P} + e\vect{A}\right)\cdot \vect{q} \ne 0 $, the spectral point separating propagating and tunneling regimes depends on helicity. As a result, at given $\epsilon$ and $\vect{P}$, one may have $k_+^2 k_-^2 < 0$, that is only particles with one helicity may propagate \cite{bai_wavevector_2016,bai_chiral_2016}. The boundary between the regions of the phase space where only one helicity is attenuated is a plane $F(\epsilon, \vect{P}) = 0$.
This suggests the way to maximize the effect of separating helicities based on non-overlapping bands. Let, for instance, $q_0 = 0$, then choosing the scalar potential such that $\widetilde{\epsilon} \approx 0$ makes states with $\xi = 1$ propagating through the layer only when $\vect{P} $ is in a narrow vicinity of $-\vect{q} - \vect{A}$, while $\xi = -1$ states propagate when $\vect{P} \approx \vect{q} - \vect{A} $. The characteristic feature of discriminated helicities due to mismatched bands is the separation between momenta 
corresponding to propagating modes with different helicities proportional to the separation between the Weyl points in the momentum space.

A phenomenon of a different origin is ``Klein filtering'', which is due to the tight relation between the spin of the electron and direction of its propagation and thus is inherently connected to helicity. From the transport point of view, this phenomenon is due to strong variation of transmission near the direction corresponding to Klein tunneling \cite{katsnelson_chiral_2006,tudorovskiy_chiral_2012,reijnders_semiclassical_2013,kleptsyn_chiral_2015}. As we show below, separation of helicities in this case occurs regardless of mutual band arrangement. 

For a three layer system, the scattering problem is naturally set in terms of the states introduced above. For a chosen helicity $\xi$, the transmission and reflection coefficients are found from
\begin{equation}\label{eq:scat_prob}
 \widehat{T}_\xi \left( \ket{\vect{h}^{(+)}_\xi(1)} + r_\xi \ket{\vect{h}^{(-)}_\xi(1)}\right) = t_\xi \ket{\vect{h}^{(+)}_\xi(1)}.
\end{equation}
Here and below, $1$ and $2$ in the parentheses denote the semi-infinite and middle layers, respectively, described by the sets of parameters ${q}_0(1,2) $, $\vect{q}(1,2) $, $\vect{A}(1,2) $ and $V(1,2) $. 
Using Eq.~\eqref{eq:states_prop}, we obtain
\begin{equation}\label{eq:trans_step1}
\begin{split}
t_\xi^{-1} = & \matrixel{-\vect{e}_x }{\rme^{-\beta \widetilde{\vect{P}}_\xi \cdot \bosi/2} \widehat{T}_\xi \rme^{\beta \widetilde{\vect{P}}_\xi \cdot \bosi/2}}{-\vect{e}_x } \\
%
& = \cos(k_\xi(2) d) - \rmi \sin(k_\xi(2)d) D_\xi,
\end{split}
\end{equation}
where
\begin{equation}\label{eq:D_par}
 D_\xi = \frac{1}{k_\xi(2)k_\xi(1)} \left[ \widetilde{\epsilon}_\xi(1)\widetilde{\epsilon}_\xi(2) - \widetilde{\vect{P}}_\xi(1) \cdot \widetilde{\vect{P}}_\xi(2) \right].
\end{equation}
From $|t_\xi|^2 + |r_\xi|^2 = 1$, it follows that
\begin{equation}\label{eq:r_prop}
 |r_\xi|^2 \propto \left| D_\xi \right|^2 - \sign(k_\xi^2(2)).
\end{equation}
Using this expression, it can be shown that when $k_\xi^2(2) > 0$, reflection vanishes if
$\widetilde{\epsilon}_\xi(1) \widetilde{\vect{P}}_\xi(2) - \widetilde{\epsilon}_\xi(2) \widetilde{\vect{P}}_\xi(1) = 0$. For helicity $\xi$ this condition is fulfilled when $\vect{P} = \vect{P}^{(K)}_\xi $ with
\begin{equation}\label{eq:PKT_def}
% \begin{split}
\vect{P}^{(K)}_\xi =  \frac{1}{\Delta U_\xi} \left[ \widetilde{\epsilon}_\xi(2) \vect{Q}_\xi(1) - \widetilde{\epsilon}_\xi(1) \vect{Q}_\xi(2) \right],
\end{equation}
where $\Delta U_\xi = U_\xi(2) - U_\xi(1)$.

Using the expression for $\vect{P}^{(K)}_\xi $, the scattering coefficients can be presented in a compact form
\begin{equation}\label{eq:tr_compact}
%\begin{split}
|t_\xi|^2 = \frac{1}{1 + f_\xi^2(\vect{P})}, \quad
%
|r_\xi|^2 = \frac{f_\xi^2(\vect{P})}{1 + f_\xi^2(\vect{P})},
%\end{split}
\end{equation}
where for $k_\xi^2(2) > 0$ we have
\begin{equation}\label{eq:f_s_param}
f_\xi^2(\vect{P}) = \left( \vect{P} - \vect{P}_\xi^{(K)} \right) \cdot \tensor{X}_\xi^2 \cdot \left( \vect{P} - \vect{P}_\xi^{(K)} \right)
\end{equation}
with tensor
\begin{equation}\label{eq:X_tensor}
\tensor{X}_\xi^2 = \frac{\sin^2 \left( k_\xi(2) d \right)  }{k^2_\xi(1) k^2_\xi(2)} \left[ \Delta U_\xi^2 + \Delta Q_\xi^2 - \Delta \vect{Q}_\xi \otimes \Delta \vect{Q}_\xi \right] , 
\end{equation}
where $\Delta \vect{Q}_\xi = \vect{Q}_\xi(2) - \vect{Q}_\xi(1) $. 

These equations reveal the phenomenon of Klein tunneling, vanishing reflectivity for arbitrary $d$, when the momentum of incoming electron satisfies $\vect{P} = \vect{P}^{(K)}_\xi $. It should be emphasized that this phenomenon is different from the full transmission at ``magic angles'' \cite{kleptsyn_chiral_2015}. In terms of expressions written above, the magic angles correspond to vanishing $\tensor{X}_\xi$, which occurs when $k_\xi(2) d = \pi n$ with integer $n$. Thus, the magic angles are essentially Fabry-Perot resonances, while the Klein tunneling is due to matching of the spin states inside and outside of the barrier and corresponds to non-reflecting \emph{interfaces}.

In order to find the condition for appearing the Klein tunneling in the transmission spectrum, one needs to take into account that $\vect{P} $ is constrained by the requirement that outside of the middle layer we have propagating modes. This leads to
\begin{equation}\label{eq:KT_exist}
\Delta Q_\xi < |\Delta U_\xi|.
\end{equation}
The obtained expressions show that, generally speaking, the in-plane momentum corresponding to Klein tunneling depend on helicities of the electron. In order to explicate this dependence, we rewrite Eq.~\eqref{eq:PKT_def} as
\begin{equation}\label{eq:KT_helicity}
 \vect{P}_\xi^{(K)} = \bar{\vect{P}}^{(K)} + \xi \Delta \vect{P}^{(K)},
\end{equation}
where
\begin{equation}\label{eq:KT_parts}
\begin{split}
 \bar{\vect{P}}^{(K)} = - \bar{\vect{A}} - \frac{1}{\Delta \epsilon^2 - \Delta q_0^2}
        \left( g \Delta \vect{A} - h \Delta \vect{q} \right), \\
%
  \Delta \vect{P}^{(K)} = - \bar{\vect{q}} + \frac{1}{\Delta \epsilon^2 - \Delta q_0^2}
        \left( h \Delta \vect{A} - g \Delta \vect{q} \right), 
\end{split}
\end{equation}
where $g = \bar{\epsilon} \Delta \epsilon + \bar{q}_0 \Delta q_0 $ and $h = \bar{\epsilon} \Delta q_0 + \bar{q}_0 \Delta \epsilon$ with the bar denoting the mean value of the respective parameters across terminating and middle layers and $\Delta$ denoting the difference between them: $\bar{\vect{A}} = [\vect{A}(2) + \vect{A}(1)]/2 $, $\Delta{\vect{A}} = \vect{A}(2) - \vect{A}(1) $, $\bar{\vect{q}} = [\vect{q}(2) + \vect{q}(1)]/2 $, $\Delta{\vect{q}} = \vect{q}(2) - \vect{q}(1) $, $\bar{{q}}_0 = [{q}_0(2) + {q}_0(1)]/2 $, $\Delta{{q}}_0 = {q}_0(2) - {q}_0(1) $, $\bar{\epsilon} = \epsilon - e[V(2) + V(1)]/2 $, $\Delta \epsilon = e [V(2) - V(1)] $. 

Equations~\eqref{eq:KT_helicity} and~\eqref{eq:KT_parts} are one of the main results of the paper. They show that propagation through the barrier is a result of an effective coupling between the helicity state of the Weyl fermion and external fields despite the absence of the direct coupling in the Hamiltonian. The origin of such coupling is the sole nature of helicity as a tight relation between momentum of the particle and its spin. The Klein tunneling is a phenomenon especially suited for revealing such coupling. The Klein tunneling occurs when the spinor eigenstates are homogeneous across the structure, while the orientation of the spinors depends on local values of both external fields and separations between the Weyl points. 

As follows from Eqs.~\eqref{eq:KT_helicity} and~\eqref{eq:KT_parts}, helicity-dependent Klein tunneling can be achieved in a variety of systems and can be controlled by parameters characterizing the barrier. For example, if the Weyl points are only separated in the momentum space with $\vect{q}(x) \equiv \vect{q} $, a potential barrier satisfying Eq.~\eqref{eq:KT_exist} yields Klein tunneling for different helicities occurring at in-plane momenta separated by $\vect{P}_-^{(K)} - \vect{P}_+^{(K)} = 2 \vect{q} $, similar to what we have seen in the case of mismatching propagation and tunneling regimes. At the same time, it must be noted that separation between the Weyl points in the momentum space is not a necessary condition for distinct Klein tunneling directions. Indeed, if the Weyl points have different energies but correspond to the same momentum, so that $\vect{q}(x) \equiv 0 $ and $q_0(x) \equiv q_0$, one has $\vect{P}_-^{(K)} - \vect{P}_+^{(K)} = 2 \Delta\vect{A} q_0/\Delta \epsilon $, so that a matrix potential created by both, scalar and vector, potentials may induce helicity dependent Klein tunneling. Finally, in WSM with Weyl points separated both in energy and momentum, by varying the scalar component of the matrix potential, one can either enhance the separation between $\vect{P}_\pm^{(K)}$ or reduce it.

We complete the consideration of transmission properties, by a brief discussion of the case, when the electron tunnels through the middle layer, $k_\xi(2) = i \kappa_\xi$. In this case, as follows from Eq.~\eqref{eq:r_prop}, the reflection coefficient is alway positive and the Klein tunneling does not take place. Equation~\eqref{eq:tr_compact} can be shown to remain valid with substituted $f^2_\xi \to f^2_\xi + 2 \sinh^2(\kappa_\xi d) $, so that the transmission coefficient demonstrates asymptotic monotonous exponential decay with $d$. The discrimination between helicities, in this case, is mostly due to their non-coinciding propagating and tunneling bands as discussed above. In this case, we can represent
\begin{equation}\label{eq:fP_tunnel}
 f_\xi^2(\vect{P}) = \left( \vect{P} - \vect{S}_\xi^{*} \right) \cdot \tensor{X}_\xi^2 \cdot \left( \vect{P} - \vect{S}_\xi \right),
 \end{equation}
where $\vect{S} = \vect{P}_\xi^{(K)} + \rmi \Delta \vect{S}_\xi $ with vector $\Delta \vect{S}_\xi$ perpendicular to $\Delta\vect{Q}_\xi $ and magnitude
\begin{equation}\label{eq:DS_mag}
 \Delta S_\xi^2 = \frac{2\kappa^2_\xi(2) k_\xi^2(1)}{ \Delta U_\xi^2 + \Delta Q_\xi^2 }.
\end{equation}
Since the resonant vector $\vect{S}_\xi$ is complex, it is never reached and, for a generic $d$, transmission is determined by the tails of the Lorentz distribution.

\section{Helicity filtering}
\label{sec:helicity_filtering}

Helicity-dependent directions of Klein tunneling leads to the effect of helicity filtering even when energies correspond to propagating modes of both helicities ($k_\xi^2(x) > 0$). We illustrate this effect by considering an ensemble of incoming states characterized by the same energy $\epsilon$. The in-plane momentum, $\vect{P}$, is limited by the propagation condition $|\vect{P} + \vect{Q}_\xi(1) | < |\widetilde{\epsilon}_\xi(1) |$, which defines a circle in the $\vect{P} $-plane, centered at $-\vect{Q}_\xi(1) $ and with the radius $\sqrt{\widetilde{\epsilon}_\xi^2(1) + Q_\xi^2(1)} $. Applying Eq.~\eqref{eq:tr_compact}, we can see that the Klein tunneling manifests as a resonance in the transmission spectrum, as is illustrated in Fig.~\ref{fig:xi_resonance} for the particular case, when in the terminating layers the Weyl points are not separated, $\vect{q}_1 = 0 $.

\begin{figure}[t]
  \centering
  \includegraphics[width=3.5in]{Transmission_Spectra.png}
  \caption{The dependence of transmission through a narrow (a) and wider (b) WSM layer with $\bar{\vect{P}}^{(K)} = 0 $ on the in-plane component of momentum for an electron with positive helicity and fixed energy $\epsilon \gtrsim V(2)$.
  %$d_b/d_a = 1.5$
  }
  \label{fig:xi_resonance}
\end{figure}

As follows from Eqs.~\eqref{eq:f_s_param} and \eqref{eq:X_tensor}, the resonance has approximately an elliptical shape in the $\vect{P} $-plane with the major axis oriented perpendicularly to $\Delta\vect{Q}_\xi $. We consider the case, when the potential inside the middle layer is chosen such that $k_\xi(1) $ is small. Disregarding the non-circular shape of the resonance, we approximate
\begin{equation}\label{eq:tran_Lorentz}
 |t_\xi|^2 \sim \frac{\Delta P_c^2}{\Delta P_c^2 + \Delta P^2 },
\end{equation}
with $\Delta P_c^2 \approx \frac{k_\xi^2(1)}{d^2} \left[e^2 \Delta V^2 + \Delta Q_\xi^2  \right]^{-1}  $, where $k_\xi^2(1)$ is evaluated at $\vect{P}^{(K)}_\xi $. Thus, the width of the resonance linearly decreases with $d$ and $|\Delta V|$ (cf. Fig.~\ref{fig:xi_resonance}(a) and \ref{fig:xi_resonance}(b)) and for sufficiently narrow resonances the transmission may demonstrate significant reduction away from the resonant point. It should be noted, however, that with increasing $V(2)$ the approximation of small $k_\xi(2)$ breaks down and resonances may demonstrate complex structure due to Fabry-Perot resonances (Fig.~\ref{fig:angular}(b,c)).

As a result, the flux of transmitted electrons demonstrates strong helicity imbalance, $(|t_+|^2 - |t_-|^2)/(|t_+|^2 + |t_-|^2)$, as illustrated by Fig.~\ref{fig:angular}(a). It shows that the space of accessible momenta in the $(y,z)$-plane is separated into two regions with well-defined dominant helicity of the outgoing particles. Since for a fixed energy the in-plane momentum unambiguously determines the direction of propagation, Fig.~\ref{fig:angular}(a) shows that effectively helicities of the outgoing particles are determined by their direction of propagation. The transition region between the subspaces with imbalanced helicities depends on the width of the barrier and is situated near the line $(\vect{P} + e \vect{A})\cdot \vect{q} = 0 $, along which there is a symmetry between different helicities. In order to show the effect of helicity discrimination in more details, we consider the case when $\vect{q}(1) = 0 $, $V(1) = 0$ and $\vect{A}(x) \equiv 0$, when
 $\vect{P}_\xi^{(K)} = -\xi \epsilon \vect{q}(2)/V(2)$. Thus, if ${P}_\xi^{(K)} > \Delta P_c$, with $\Delta P_c = k(1) k(2) \left[ \sin(k(2)d) \sqrt{V^2(2) + q^2(2)} \right]^{-1}  $, the transmission resonances for different  helicities are well separated and the middle layer can be regarded as transmitting states of definite helicities. This is illustrated by Fig.~\ref{fig:angular}(b,c), where the transmission is shown, for the case $\bar{\vect{P}}^{(K)} = 0 $, as a function of the angle of incidence in the plane spanned by $\vect{e}_x$ and $\Delta \vect{P}^{(K)}$, that is when the in-plane momentum varies along the line $\vect{P}(\lambda) = \vect{P}_+^{(K)} + \lambda \Delta \vect{P}^{(K)} $. It is seen that the angular spectrum demonstrates well-defined directionality when the variation of $k_\xi(2)d$ as a function of $\vect{P} $ is significant on the scale determined by the eigenvalues of $\tensor{X}^{-1}_\xi(\vect{P} = \vect{P}^{(K)}_\xi) $. With increasing $V(2)$ or $d$, additional directional resonances, corresponding to the ``magic angles'', appear along directions where $k_\xi(2)d = \pi n$ with integer $n$. 

\begin{figure}[tb]
  \centering
  \includegraphics[width=3.2in]{Angular_Spectra.png}
  \caption{
(a) Helicity imbalance, $(|t_+|^2 - |t_-|^2)/(|t_+|^2 + |t_-|^2)$, as a function of the in-plane momentum. (b,c) The angular dependence of transmission of electrons with (a) $\xi = 1$ and (b) $\xi = -1$ through narrow (solid line) and wide (dashed line) barriers demonstrating complete transmission due to Klein tunneling and a Fabry-Perot resonance, respectively.}
  \label{fig:angular}
\end{figure}


The helicity filtering effect impacts the current plowing through the barrier. The probability current carried by state $\psi $ is $\vect{j}(\psi) = \psi^\dagger \boal \psi$. Thus, for a mixture of scattering states at energy $\epsilon$, we have
\begin{equation}\label{eq:inc_cur}
 \vect{j} = \int d \vect{P}\, \rho(\vect{P}; \epsilon) \vect{j} \left[ \psi(\vect{P}) \right],
\end{equation}
where $\rho(\vect{P}; \epsilon)$ are the diagonal elements of the density matrix describing the distribution of states within the circle of admissible in-plane momenta and $\psi(\vect{P}) $ are scattering states corresponding to the in-plane momentum $\vect{P} $. Behind the barrier, the current is carried by the outgoing states only and, therefore, separating contributions stemming from different helicities we obtain
\begin{equation}\label{eq:inc_cur_sep}
\begin{split}
 \vect{j} = & -\sum_\xi \xi 
    \int d \vect{P}\, \rho(\vect{P}; \epsilon) 
                \left | t_\xi (\vect{P}) \right |^2
                \matrixel{\vect{h}^{(+)}_\xi(\vect{P})}{\bosi}{\vect{h}^{(+)}_\xi(\vect{P})} \\
%
        & = \sum_\xi \int d \vect{P}\, \rho(\vect{P}; \epsilon) 
                \left | t_\xi (\vect{P}) \right |^2
                \vect{e}_\xi(\vect{P}),
\end{split}
\end{equation}
where $\vect{e}_\xi(\vect{P}) = -\xi \vect{h}^{(+)}_\xi(\vect{P})/\widetilde{\epsilon}_\xi $ [see Eq.~\eqref{eq:eff_field}].

In order to discuss the main effect, we assume that the resonances corresponding to Klein tunneling are sufficiently narrow, so that we can approximate
\begin{equation}\label{eq:comp_cur}
 \vect{j} = \rho_+(\vect{P}^{(K)}_+) \vect{e}_+(\vect{P}^{(K)}_+) + 
                \rho_-(\vect{P}^{(K)}_-) \vect{e}_-(\vect{P}^{(K)}_-).
\end{equation}
In other words, particles with different helicities flow along, generally speaking non-coinciding, directions specified by vectors
\begin{equation}\label{eq:flux_directions}
 \vect{e}_\xi = \frac{1}{\epsilon} \left( k_\xi \vect{e}_x + \widetilde{\vect{P}}^{(K)}_\xi \right),
\end{equation}
where $\widetilde{\vect{P}}^{(K)}_\xi$ is given by Eq.~\eqref{eq:KT_helicity} with substituted $-\bar{\vect{A}} \to \Delta \vect{A} $ and $-\bar{\vect{q}} \to \Delta \vect{q} $.


The weight of contributions with different helicities is determined by the population of states with in-plane momenta corresponding to Klein tunneling. These momenta, in turn, can be controlled by external field in such way that the region of admissible in-plane momenta may contain the resonant momentum only for one helicity. This effectively cancels the contribution of another helicity into outgoing current, making the current helicity-polarized. It must be noted that removal of the resonant momentum from the circle of admissible momenta does not imply that there are no propagating modes with the respective helicity at energy $\epsilon$ but rather signifies strong reflection of electrons with blocked helicity from the barrier.

\section{Conclusion}

An important feature distinguishing Weyl semimetals (WSM) from other Dirac materials is the separation between Weyl points with different chiralities. From the dynamical point of view, this  introduces a matrix potential, each own for states with different helicities. When the separation is only in energy, the potential becomes purely scalar, while in the case of the separation only in the momentum space, the potential is vector. These potentials induce non-coinciding dynamics of states with different helicities. This provides opportunities to control the spatial flow of helicities in the \textit{absence} of an external field acting directly on helicity. 

We demonstrate the above property of WSM by considering transport properties of a multilayer Weyl semimetal structure formed by piece-wise constant potentials and separations between the Weyl points. In the case when there are no parallel electric and magnetic fields, so that the helicity fully conserves, there are two physical mechanisms discriminating between helicities. One is due to the dependence of the edge of the band on helicity. As a result, there are (direction dependent) energy regions, where the transmitted state is due to propagating modes inside the barrier for one helicity and due to tunneling for the opposite one. 

We have shown, that the Klein tunneling provides additional mechanism of helicity selection. Using the formalism of spinor transfer matrices, we have found that there are directions, generally speaking helicity dependent, along which the electron propagates through the barrier with certainty regardless the width of the barrier. Near such resonance direction, the transmission regarded as a function of the in-plane momentum for a fixed energy is shown to be approximately of a Lorentzian form. Thus, when the directions of Klein tunneling for different helicities are well separated comparing to the width of the transmission resonance, the outgoing states are characterized by a strong helicity mismatch.

\section*{Acknowledgements} 

The work was supported by the Air Force Office of Scientific Research (AFOSR) Grant No. FA9550-16-1-0363. 

\bibliography{weyl_helicity.bib}

\end{document}
