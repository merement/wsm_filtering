## Plots the effect of a layer/interface on distribution over momenta

## 4. Angular spectrum of helicity: section through the Klein point

from __future__ import division
#from __future__ import print_function

from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import matplotlib.pyplot as plt
import numpy as np

import cmath

############# The following code is taken from 
### http://stackoverflow.com/questions/26137222/a-half-polar-plot-directivity-pattern-with-db-scale-and-negative-angle-values

## """Demo of polar plot of arbitrary theta. This is a workaround for MPL's polar plot 
## limitation to a full 360 deg.
## 
## Based on http://matplotlib.org/mpl_toolkits/axes_grid/examples/demo_floating_axes.py
## """


from matplotlib.transforms import Affine2D
from matplotlib.projections import PolarAxes
from mpl_toolkits.axisartist import angle_helper
from mpl_toolkits.axisartist.grid_finder import MaxNLocator
from mpl_toolkits.axisartist.floating_axes import GridHelperCurveLinear, FloatingSubplot


def fractional_polar_axes(f, thlim=(0, 180), rlim=(0, 1), step=(30, 0.2),
                          thlabel='theta', rlabel='r', ticklabels=True):
    """Return polar axes that adhere to desired theta (in deg) and r limits. steps for theta
    and r are really just hints for the locators."""
    th0, th1 = thlim # deg
    r0, r1 = rlim
    thstep, rstep = step

    # scale degrees to radians:
    tr_scale = Affine2D().scale(np.pi/180., 1.)

    tr = tr_scale +  PolarAxes.PolarTransform()

    theta_grid_locator = angle_helper.LocatorDMS((th1-th0)//thstep)
    r_grid_locator = MaxNLocator((r1-r0)//rstep)
    theta_tick_formatter = angle_helper.FormatterDMS()

    grid_helper = GridHelperCurveLinear(tr,
                                        extremes=(th0, th1, r0, r1),
                                        grid_locator1=theta_grid_locator,
                                        grid_locator2=r_grid_locator,
                                        tick_formatter1=theta_tick_formatter,
                                        tick_formatter2=None)

    a = FloatingSubplot(f, 111, grid_helper=grid_helper)
    f.add_subplot(a)
     # adjust x axis (theta):
    a.axis["bottom"].set_visible(False)
    a.axis["top"].set_axis_direction("bottom") # tick direction
    a.axis["top"].toggle(ticklabels=ticklabels, label=bool(thlabel))
    a.axis["top"].major_ticklabels.set_axis_direction("top")
    a.axis["top"].label.set_axis_direction("top")

    # adjust y axis (r):
    a.axis["left"].set_axis_direction("bottom") # tick direction
    a.axis["right"].set_axis_direction("top") # tick direction
    a.axis["left"].toggle(ticklabels=ticklabels, label=bool(rlabel))

    # add labels:
    a.axis["top"].label.set_text(thlabel)
    a.axis["left"].label.set_text(rlabel)

    # create a parasite axes whose transData is theta, r:
    auxa = a.get_aux_axes(tr)
    # make aux_ax to have a clip path as in a?:
    auxa.patch = a.patch 
    # this has a side effect that the patch is drawn twice, and possibly over some other
    # artists. So, we decrease the zorder a bit to prevent this:
    a.patch.zorder = -2

    # add sector lines for both dimensions:
    thticks = grid_helper.grid_info['lon_info'][0]
    rticks = grid_helper.grid_info['lat_info'][0]
    for th in thticks[1:-1]: # all but the first and last
        auxa.plot([th, th], [r0, r1], '--', c='grey', zorder=-1)
    for ri, r in enumerate(rticks):
        # plot first r line as axes border in solid black only if it isn't at r=0
        if ri == 0 and r != 0:
            ls, lw, color = 'solid', 2, 'black'
        else:
            ls, lw, color = 'dashed', 1, 'grey'
        # From http://stackoverflow.com/a/19828753/2020363
        auxa.add_artist(plt.Circle([0, 0], radius=r, ls=ls, lw=lw, color=color, fill=False,
                        transform=auxa.transData._b, zorder=-1))
    return auxa

############


# Some general constants

# "Zero"

tol = 1.0e-7

# Pauli matrices, cause why not
 
sigma_x = np.array([[0, 1.0], [1.0, 0]], dtype = complex)
sigma_y = np.array([[0, -1j*1.0], [1j*1.0, 0]], dtype = complex)
sigma_z = np.array([[1.0, 0], [0, -1.0]], dtype = complex)

sigma_0 = np.array([[1.0, 0], [0, 1.0]], dtype = complex)

# some universal states

vx = np.array([1.0, 1.0], dtype = complex)/np.sqrt(2)
vxm = np.array([1.0, -1.0], dtype = complex)/np.sqrt(2)

# some bases vectors in 3D 

ex = np.array([1.0, 0.0, 0.0], dtype = complex)

def v2psi(v) :
    """ Convert given vector to spinor amplitudes
        The coherent state is defined as a solution of an equation
        \epsilon \ket{v} = v . \sigma \ket{v}
        We define amplitudes of the coherent states directly from this definition
        
        INPUT: v - a 3D vector
    """

    eps = sum(v*v) # here's the "energy"
    psi1 = v[0] - 1j*v[1]
    psi2 = eps - v[2]

    # Then norm and normalization
    norm = sqrt(abs(psi1)**2 + abs(psi2)**2)
    psi1 = psi1/norm
    psi2 = psi2/norm

    return np.array([psi1, psi2])

def prod_coherent (v1, v2) :
    """Return the scalar product of two coherent states defined 
    by vectors v1 and v2. The vectors are not assumed to be real.
    
    INPUT: v1, v2 - two 3D vectors (numpy arrays) with (generally) 
            complex components
    """

    return sum(v2psi(v1).conj()*v2psi(v2))

# END of prod_coherent

def prod_psi (psiL, psiR) :
    """ Take the scalar product of two states 

    < psiL | psiR > = \sum_j psiL[j]*psiR[j]

    Notice that there's no conjugation. State psiL is taken 
    as is for the evaluation.

    INPUT: psiL, psiR - two 2D spinors (numpy arrays)
    """

    return sum(psiL*psiR)

# END of prod_psi   

def psi_states (hplus, hminus) :
    """ Return direct and dual states based hplus, hminus
    Notice that dual states are returned "conjugated"
    """

    psiplus = v2psi(hplus)
    psiminus = v2psi(hminus)

    hbarplus = -hminus
    hbarminus = -hplus

    psibarplus = v2psi(hbarplus).conj()
    psibarminus = v2psi(hbarminus).conj()

    norm1 = prod_psi(psibarplus, psiplus)
    norm2 = prod_psi(psibarminus, psiminus)

    psibarplus = psibarplus/norm1
    psibarminus = psibarminus/norm2

    return (psiplus, psiminus, psibarplus, psibarminus)

# END of psi_states 

    
def int_transfer (xi, k, P1, P2):
    """ Return the matrix elements of the interface transfer matrix

    INPUT:  xi - helicity (\pm 1)
            k - wave number (scalar) along the x-axis
            P1, P2 - two 3D vectors (numpy arrays) describing in-plane 
                    momenta within each layer: P1 <- (n), P2 <- (n + 1).
                    They must have first component zero (we assume that 
                    the gauge transformation has already taken care of 
                    this part).
    OUTPUT: M - 2x2 matrix (numpy array)
    """

    # Elementary data validation
    if abs(P1[0]) > tol :
        print "Warning: P1 has the x-component out of bound!"
        P1[0] = 0.0
    if abs(P2[0]) > tol :
        print "Warning: P1 has the x-component out of bound!"
        P2[0] = 0.0

    # First we construct vectors specifying interiors of the layers

    ex = np.array([1.0, 0, 0], dtype = complex)
    h2plus = -xi*k*ex - xi*P2
    h2minus = xi*k*ex - xi*P2

    h1plus = -xi*k*ex - xi*P1
    h1minus = xi*k*ex - xi*P1


    # find the direct states for n and the dual state for n+1

    (psiplus, psiminus, p1, p2) = psi_states(h1plus, h1minus)
    (p1, p2, psibarplus, psibarminus) = psi_states(h2plus, h2minus)

    # now we evaluate the matrix elements

    T11 = prod_coherent(psibarplus, psiplus)
    T12 = prod_coherent(psibarplus, psiminus)
    T21 = prod_coherent(psibarminus, psiplus)
    T22 = prod_coherent(psibarminus, psiminus)

    return np.array([[T11, T12], [T21, T22]])

# END of int_transfer

def layer_transfer(k, d):

    return np.array([[cmath.exp(1j*k*d), 0], [0, cmath.exp(-1j*k*d)]])

def EXP(v, u = 0) :
    """Evaluate the exponent of matrix 

    v \cdot \sigma = u + \sum_i v_i \sigma_i.

    INPUT: v - 3-vector (numpy array), u - scalar
    OUTPUT: 2x2 matrix (numpy array)

    Comments:
    exp (v \sigma) = exp(u) M,

    where

    M = cosh(theta) + v_i \sigma_i/theta sinh(theta)

    with theta = sqrt{\sum_i v_i^2}

    """

    theta = cmath.sqrt(sum(v*v))

    mm = v[0]*sigma_x + v[1]*sigma_y + v[2]*sigma_z 
    # print v, 'theta : ', theta

    M = cmath.cosh(theta)*sigma_0 + cmath.sinh(theta)*mm/theta

    return cmath.exp(u)*M

# END of EXP

def CROSS(v1, v2) :
    """ Evaluate the "cross product" of two vectors

    INPUT: v1, v2 - two 3D vectors (numpy arrays)
    OUTPUT: 3D vector = v1 \times v2

    """

    v = np.array([0.0, 0.0, 0.0 ], dtype = complex)

    v[0] =  v1[1]*v2[2] - v1[2]*v2[1]
    v[1] =  v1[2]*v2[0] - v1[0]*v2[2]
    v[2] =  v1[0]*v2[1] - v1[1]*v2[0]

    return v

# END of CROSS

## Evaluation of reflection and transmission spectra

def fieldpar (structure) :
    # return the main parameters characterizing layers out of the data structure
    P = structure['P']
    q1 = structure['q1']
    q2 = structure['q2']

    eps1 = structure['eps1']
    eps2 = structure['eps2']

    P_K = (eps2*q1 - eps1*q2)/(eps1 - eps2)

    h1yz = P + q1
    h1yzm = np.sqrt(sum(h1yz*h1yz))

    # print h1yz

    h2yz = P + q2
    h2yzm = np.sqrt(sum(h2yz*h2yz))

    # print h2yz

    # now we construct h_fields
    h1x = np.sqrt(eps1**2 - h1yzm**2)

    if abs(h1x.imag) > tol :
        raise ValueError('Modes outside must be propagating!', structure)

    # notice that in the middle layer we can have attenuation
    h2x = cmath.sqrt(eps2**2 - h2yzm**2)
    
    return (eps1, eps2, h1yz, h1x, h2yz, h2x, P_K)

def trans (structure) :
    """ Evaluate for a given structure:
        reflection, transmission, external effective field, transfer matrix 
    """

    (eps1, eps2, h1yz, h1x, h2yz, h2x, _) = fieldpar(structure)

    d = structure['d']

    h1yzm = h1yz.dot(h1yz)
    h2yzm = h2yz.dot(h2yz)
    
    # now we construct h_fields

    h1 = h1x*ex + h1yz
    h2 = h2x*ex + h2yz

    Tlayer = EXP(1j*eps2*d*ex - d*CROSS(h2yz, ex))
    
    if abs(h1x) < tol :
        print "The secular regime!"
        h1x = tol

    beta = np.arcsinh(h1yzm/h1x)/h1yzm

    sideleft = EXP(-beta*h1yz/2)
    sideright = EXP(beta*h1yz/2)

    Ttot = sideleft.dot(Tlayer).dot(sideright)

    denom = Ttot.dot(vxm).dot(vxm)

    num = Ttot.dot(vx).dot(vxm)

    R = abs(num/denom)**2
    T = abs(1/denom)**2

    return (R, T, h1x, Ttot)

def transF(structure) :
    # return transmission and reflection evaluated according to analytical formulas
    #
    P = structure['P']
    q1 = structure['q1']
    q2 = structure['q2']
    d = structure['d']

    (eps1, eps2, h1yz, h1x, h2yz, h2x, P_K) = fieldpar(structure)
    dP = P - P_K

    # are we in the propagating regime outside

    app = 2.0*abs(np.sin(h2x*d)**2) if abs(h2x.imag) > tol else 0

    dQ = h2yz - h1yz

    XXf2 = ((eps1 - eps2)**2 + dQ.dot(dQ))*np.identity(3, dtype = complex) - np.outer(dQ,dQ)
    XXf1 = np.sin(h2x*d)**2/(h1x**2 * h2x**2)
    XX = XXf2*XXf1

    f2 = XX.dot(dP).dot(dP) + app

    t = 1/(1 + f2)
    r = f2/(1 + f2)
    return (r, t, h1x, h2x)
    
## END OF Evaluation of reflection and transmission spectra

def Ppatch(m1, app) :
    P1_min = m1 - app
    P2_min = -m1 - app

    P_min = P1_min if P1_min > P2_min else P2_min

    P1_max = m1 + app
    P2_max = -m1 + app

    P_max = P1_max if P1_max < P2_max else P2_max

    return (P_min, P_max)

def PRan(struct,phi) :
    # Find the admissible range of in-plane momenta for both helicities

    q1 = struct['q1']
    q1m = np.sqrt(q1.dot(q1))

    m1 = -q1m*np.cos(phi)
    app = np.sqrt(eps1**2 - q1m**2*np.sin(phi)**2)

    (P_min, P_max) = Ppatch(m1, app)

    if P_max < P_min :
        raise ValueError('Helicities are discriminated', struct, phi)

    return (P_min, P_max)

def PRanV(struct, ep, P_K) :
    # Find the admissible range along particular direction
    #
    # P = P^{(K)}_+ + \lambda (P^{(K)}_- - P^{(K)}_+)

    epm = ep.dot(ep)

    P1 = P_K + struct['q1']
    P1p = ep.dot(P1)/epm
    app1 = np.sqrt(struct['eps1']**2/epm - P1p**2 + (P1.dot(P1)/epm)**2)
    l1_min = -P1p - app1
    l1_max = -P1p + app1

    P2 = P_K - struct['q1']
    P2p = ep.dot(P2)/epm
    app2 = np.sqrt(struct['eps1']**2/epm - P2p**2 + (P2.dot(P2)/epm)**2)

    l2_min = -P2p - app2
    l2_max = -P2p + app2

    print 'l1_set : ', l1_min, l1_max, ' l2_set : ', l2_min, l2_max

    l_min = l1_min if l1_min > l2_min else l2_min
    l_max = l1_max if l1_max < l2_max else l2_max

    print 'l_minmax : ', l_min, l_max

    if l_max < l_min :
        raise ValueError('Helicities are discriminated', struct, eP, P_K)

    return (l_min, l_max)

def angles(k_ar, p_ar) :
    # Return array of angles in degrees out of two arrays with in-plane and out-of-plane components

    phi_ar = []

    for kx, py in zip(k_ar, p_ar) :
        phi_ar.append(np.arctan2(py.real,kx.real)*180.0/np.pi)

    phi_ar = np.array(phi_ar).real

    return phi_ar

def tsection(struct, PKT, eK, lran, choice = 1) :
    """ Evaluate section of the transmission spectrum along eK starting from PKT with lran points

    RETURN: (py_ar, kk_ar, TT_ar, RR_ar)
    """

    # Here we choose the transmission function (transfer matrix vs explicit formulas)
    tran = transF if choice == 1 else trans

    eKm = np.sqrt(eK.dot(eK))

    RR_ar = []
    TT_ar = []    
    kk_ar = []
    py_ar = []

    for l in lran :

        P = PKT + l*eK

        struct['P'] = P
        (R, T, kx, _) = tran(struct)

        kx = kx.real
        py = (P.dot(eK)/eKm).real

        py_ar.append(py)
        kk_ar.append(kx)
        TT_ar.append(T)
        RR_ar.append(R)

        # print 'l : ', l,  '  in-plane component : ', py, ' angle : ', ph*180/np.pi

    return (np.array(py_ar), np.array(kk_ar), np.array(TT_ar), np.array(RR_ar))

# END OF tsection

## ************** MAIN PART ************** ##

dd = 28.0

epsilon  = 0.55
V1 = 0.0
V2 = 0.5

eps1 = epsilon - V1
eps2 = epsilon - V2

q1 = np.array([0.0, 0.0, 0.0], dtype=complex)
q1m = q1.dot(q1)

q2 = np.array([0.0, 0.2, 0.0], dtype=complex)

P_KT = (eps2*q1 - eps1*q2)/(V2 - V1)

print "P_KT : ", P_KT

struct = {'eps1' : eps1, 'eps2' : eps2, 'V1' : V1, 'V2' : V2, 'q1' : q1, 'q2' : q2, 'd' : dd}

struct1 = dict(struct)
struct1['q1'] = -struct1['q1']
struct1['q2'] = -struct1['q2']

## Here, we plot the angular dependence for a fixed energy

# We take into account all possibilities (accept, we've dropped A)
# P is chosen along the line connecting P^{(K)}_+ and P^{(K)}_+

P_KT = (eps2*q1 - eps1*q2)/(V2 - V1)
e_K = -2*P_KT

(l_min, l_max) = PRanV(struct, e_K, P_KT)

print 'l_min : ', l_min, ' l_max : ', l_max

P_min = P_KT + l_min*e_K
P_max = P_KT + l_max*e_K

print 'P_min : ', P_min, ' P_max : ', P_max

# there should be either neither of them or both of them

flag_Max = True if l_max > 1 else False
flag_Min = True if l_min < 0 else False

if not flag_Max :
    print "Mismatch at the top of the interval"

if not flag_Min :
    print "Mismatch at the bottom of the interval"

l_ran = np.linspace(l_min, l_max, 374)[1:-1].real

(py_ar, kk_ar, TT_ar, RR_ar) = tsection(struct, P_KT, e_K, l_ran)
(py1_ar, kk1_ar, TT1_ar, RR1_ar) = tsection(struct1, P_KT, e_K, l_ran)

dscale = 3.0
struct['d'] = dscale*dd
struct1['d'] = dscale*dd

(pyL_ar, kkL_ar, TTL_ar, RRL_ar) = tsection(struct, P_KT, e_K, l_ran)
(pyL1_ar, kkL1_ar, TTL1_ar, RRL1_ar) = tsection(struct1, P_KT, e_K, l_ran)

# Now, we restore the directions (for each helicity it's its own)

phi_ar = angles(kk_ar, py_ar)
phi1_ar = angles(kk1_ar, py1_ar)

print 'phi : ', phi_ar[0], phi_ar[-1]
print 'phi1 : ', phi1_ar[0], phi1_ar[-1]

# Plotting

f1 = plt.figure(facecolor='white')
a1 = fractional_polar_axes(f1, (-90, 90.), (0, 1.0), (30.0, 0.25),None, None, True)
#MyTitle= 'Target angle %4.1f$^\circ$' %Angle
#f1.suptitle( MyTitle, y=0.95)
a1.plot(phi_ar, TT_ar, 'r--', phi_ar, TT1_ar,'b')

f2 = plt.figure(facecolor='white')
a2 = fractional_polar_axes(f2, (-90, 90.), (0, 1.0), (30.0, 0.25),None, None, True)
#MyTitle= 'Target angle %4.1f$^\circ$' %Angle
#f1.suptitle( MyTitle, y=0.95)
a2.plot(phi_ar, TTL_ar, 'r--', phi_ar, TTL1_ar,'b')

f3 = plt.figure(facecolor='white')
a3 = fractional_polar_axes(f3, (-90, 90.), (0, 1.0), (30.0, 0.25),None, None, True)
#MyTitle= 'Target angle %4.1f$^\circ$' %Angle
#f1.suptitle( MyTitle, y=0.95)
a3.plot(phi_ar, TTL_ar, 'r--', phi_ar, TT_ar,'b')

f4 = plt.figure(facecolor='white')
a4 = fractional_polar_axes(f4, (-90, 90.), (0, 1.0), (30.0, 0.25),None, None, True)
#MyTitle= 'Target angle %4.1f$^\circ$' %Angle
#f1.suptitle( MyTitle, y=0.95)
a4.plot(phi_ar, TTL1_ar, 'r--', phi_ar, TT1_ar,'b')

plt.show()
